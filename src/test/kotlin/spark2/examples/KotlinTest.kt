package spark2.examples

import org.apache.spark.SparkConf
import org.apache.spark.api.java.JavaSparkContext
import org.junit.Test
import kotlin.reflect.KClass

class KotlinTest {

  data class MyItem(val id: Int, val value: String)

  @Test fun `kryo test`() {
    val conf = SparkConf()
        .setMaster("local")
        .setAppName("Line Counter")
        .registerKryoClasses(MyItem::class)

    val sc = JavaSparkContext(conf)

    val input = sc.parallelize(listOf(MyItem(1, "Alpha"), MyItem(2, "Beta")))

    val letters = input
        .flatMap<String> { it.value.split(Regex("(?<=.)")).iterator() }
        .map(String::toUpperCase)
        .filter { it.matches(Regex("[A-Z]")) }

    println(letters.collect())
  }
}


internal fun SparkConf.registerKryoClasses(vararg args: KClass<*>) =
    registerKryoClasses(args.map { it.java }.toTypedArray())